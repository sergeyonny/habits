package de.course.habits;

import org.junit.jupiter.api.Test;

/**
 * EvenNumbersProcessor receives a list with number scope 1-26 from NumberProvider (mock).
 * EvenNumbersProcessor returns a string with:
 * concatenated letters from german alphabet representing provided list of numbers from NumberProvider.
 * the delimiter "_" between each letter as default.
 * letters resulting from even numbers are upper-case.
 * letters resulting from odd numbers are lower-case.
 * control with an input filter avoiding specific numbers.
 * list of numbers with size > 26 received from NumberProvider produces an Exception with Message "Really heavy list of numbers".
 */
class EvenNumbersProcessorTest {
    @Test
    void should_generate_empty_letters() {
    }
}